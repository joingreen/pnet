import {
    clusterApiUrl,
    Connection,
    Keypair,
} from '@solana/web3.js';

const connection = new Connection(clusterApiUrl(import.meta.env.VITE_SOLANA_NETWORK), 'confirmed');
export const pnetwallet = Keypair.fromSecretKey(new Uint8Array([167, 251, 195, 9, 101, 143, 135, 209, 92, 70, 249, 81, 135, 79, 157, 5, 216, 249, 240, 199, 116, 171, 176, 72, 0, 177, 196, 105, 220, 154, 198, 220, 12, 61, 235, 16, 117, 65, 82, 94, 160, 200, 98, 133, 185, 211, 23, 138, 222, 199, 196, 11, 32, 181, 97, 7, 120, 150, 92, 70, 102, 162, 188, 60]))
connection.onAccountChange(
    pnetwallet.publicKey,
    (updatedAccountInfo) => console.log('Updated pnetwallet: ', updatedAccountInfo),
    'confirmed',
)
console.log(pnetwallet.publicKey.toBase58())
export default connection
