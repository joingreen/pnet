import cookie from 'cookie';
import { db, bucket } from '../../gc'
import { v4 as uuid } from '@lukeed/uuid';
import Joi from 'joi'

import vision from '@google-cloud/vision'
const CREDENTIAL = JSON.parse(JSON.stringify({
    "type": "service_account",
    "project_id": "sola-e9c81",
    "private_key_id": "df2039b1394855c35c8f99a6bdbbe5dc1b6ea798",
    "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQC7TrXaLWv/Ea3g\neA662zAxheIvKqbzOmD+8mVPoBTbA7ez/742gEyN07LTzGmVeXeXKPnEQ0pz3ERc\nVj9W6wm/YJqvzP/V0mOCnI0al7ZjpF0ll7mBD7LITGb2j0iaJ/6lM+6+P1mIXIBg\nPB3mGUeGMpdGaY3xc0eWbq/CNsvQ+gaWSXXm8zBYf3DnpkXAKj8OdJZJvLAMUpx0\nB5UU9yRLypCbDToeim6N7yEpjypybGKuZ6opj8rRhNaRmwacTD/c7JZVZzgYiBkZ\nAAZDIJprpMexshktBpdt8EwUtRIaS6LXDOXvlYrrBq81xdUCDcvytiIYqAB84/YM\nVqXTo2IzAgMBAAECggEADMcfqxzKehyZJC/chWJwOmNRR6QdE95e1pWaVAvDeyDD\noUu9jKyaTFntoFBDawHd0YSR7BHRHEVMWTuTAkQc0UmZqTY5b7tKCZN8zBx0bKi+\nUqhelyTcg+3eX6HtCIMuS9PWoh435pBfkPEY8w+0wZK69NU87ISk6hNupGJQ64ES\nOS1iywbYShgkMxgBpmd9rS1D5MnwLkZjPhe4A+O5OOpJhLIeCHZfZdm1qoueOyJz\nZGsBXYqKVAeY93mDO0lP9Sq2jD7kDQ8qEdM+oj+COkSe/WS02cUgm96p8hgtELJd\nIPsDbd40kN/3wykhrCrOt1qvIfm10D6O10XTS+wemQKBgQD3pR6g5v6ODMM5tOZ7\nz7yv8FtaF87qDRubPtkwXoYF4hn0TE1ClRkk2qId8mtptEvJxAwPRKpqp0Ex9Oje\nFuPC8jAy97VpjN31dm4cbwt9549GdzNaAna7ps/RqaI3cW8BLUbJZLgLpy5xt5Zb\nvP2VbeatIf9UrMxLJaLGql7OKQKBgQDBoHZtLtClo7Wr0FrpTb6Cs54Ls8aIcJOE\nTkD31gkltJ+lde1Spu/Uu+mNC3P8q3904LcjV5L59iSNEIZLevcZYZG8eXgcIsB9\n6mTrPcRKTuEHwpTVy3uiMYndxMiZDAL6IRh5xL9VnQGS9xiWu2bfhaMGG8gqrM79\nfUcENp1A+wKBgQDHJyLagulzkw/s+G3iY816L/FHIp5j2ua4knJXU6y0ToBQ1ovL\nX9w4pxp4uttmxnesatzH6pCh9J+ZbTtYX1ALFnlsg4iAuahGHleuxh6XwgCWyoWh\nfRm/DwdRtPmfXLnIWID0tOrNdEUFbn6ch//mi9tWoGCV1+DzDfE5kwlrAQKBgDx4\n1QO8f0V8H50NcZczxLAlUNLuzq6GGkxT9E5y9mBkTQfsELajlHuoRITcctkS5rem\nBwxuqWSPz1KPOqIQQWQX6OAoP67p25x3vBUMMWXTxMwbpUgwSOia+Cfe8Zd6/OMJ\nuw8st2URqBAyQ+evl61vmz6pJpbNacgZfDiAphRJAoGASN83WWQFrZa7HGu3YViG\nzDIqDG99RmWfezbBNZMKubqbh09FO1CbgEyi0Wa2vWeHN3raOOhz3MDSVrEzl/dg\npA17hDm0j3g8949STGkimLIEZUvjnLAFFz25x5NKRizkcBa/pNh5z0LqPAp/W8zS\nh1nS4NgOGCnyClbR79RO6J0=\n-----END PRIVATE KEY-----\n",
    "client_email": "vison-57@sola-e9c81.iam.gserviceaccount.com",
    "client_id": "118122515139711317065",
    "auth_uri": "https://accounts.google.com/o/oauth2/auth",
    "token_uri": "https://oauth2.googleapis.com/token",
    "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
    "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/vison-57%40sola-e9c81.iam.gserviceaccount.com"
}))
const CONFIG = {
    credentials: {
        private_key: CREDENTIAL.private_key,
        client_email: CREDENTIAL.client_email
    }
}
const client = new vision.ImageAnnotatorClient(CONFIG);



export async function patch(request) {
    try {
        console.log('patch /sell ')
        const cookies = cookie.parse(request.headers.cookie || '')
        if (!cookies.id) throw new Error(`Invalid`)

        const ref = db.collection('orders');
        const queryRef = ref.where('seller', '==', cookies.id).where('status', '==', 0);

        const docs = await queryRef.get()
        if (docs.empty) return { body: { data: [] } }
        let arrayData = []
        docs.forEach(doc => {
            // console.log(doc._updateTime._seconds)
            let data = doc.data()
            arrayData.push({
                attr: data.attr,
                idshoe: data.idshoe,
                level: data.level,
                mint: data.mint,
                rarity: data.rarity,
                type: data.type,

                base: data.base,
                id: data.id,
                imgs: data.imgs,
                price: data.price,
                status: data.status,
                createAt: data.createAt
            });
        });
        return { body: { data: arrayData } }

    } catch (e) {
        console.log('Error: ' + e.message)
        console.log(e.message)
        return {
            status: 400
        }
    }
}

const postschema = Joi.object({
    img: Joi.string(),
    img1: Joi.string().empty(''),
    price: Joi.number(),
    note: Joi.string().empty('')
})

export async function post(request) {
    try {
        console.log('POST /sell')
        const cookies = cookie.parse(request.headers.cookie || '')
        if (!cookies.id) throw new Error(`Invalid`)
        const body = await postschema.validateAsync(JSON.parse(request.body))

        const detectResult= await extract(body.img, body.img1)
        if(!detectResult) throw new Error('Invalid')
        
        let newdoc = {
            attr: detectResult.attributes,
            idshoe: detectResult.idshoe,
            level: parseInt(detectResult.level),
            mint: parseInt(detectResult.mint),
            rarity: detectResult.rarity,
            type: detectResult.type,
            base: detectResult.base,

            id: '',
            imgs: [],
            price: body.price,
            status: 0,
            buyer: '',
            buyeraddress: '',
            seller: cookies.id,
            createAt: Date.now()
        }
        
        let imgss = [body.img]
        if(body.img1) imgss.push(body.img1)

        imgss.forEach(async (imgString, index) => {
            const file = Buffer.from(imgString, 'base64');
            const id = uuid()
            const blob = bucket.file(`${id}.png`)
            const blobWriter = blob.createWriteStream({
                resumable: false,
            })
            blobWriter.on('error', (err) => {
                console.log(err)
            })
            blobWriter.on('finish', () => {
                console.log("File uploaded " + index)
            })
            newdoc.imgs.push(id)
            blobWriter.end(file)
        })

        newdoc.id = newdoc.imgs[0]


        console.log(newdoc)

        await db.collection('orders').doc(newdoc.id).set(newdoc)
        await db.collection('messages').doc(newdoc.id).set({
            status: 0,
            msg: []
        })
        if(body.note){
            await db.collection('messages').doc(newdoc.id).set({
            msg: [{ content: body.note || '', time: Date.now(), type: 'seller' }]
            }, { merge: true });
        }
        

        return {
            status: 200,
            body: {
                data: {
                    attr: newdoc.attr,
                    idshoe: newdoc.idshoe,
                    level: newdoc.level,
                    mint: newdoc.mint,
                    rarity: newdoc.rarity,
                    type: newdoc.type,

                    base: newdoc.base,
                    id: newdoc.id,
                    imgs: newdoc.imgs,
                    price: newdoc.price,
                    status: newdoc.status,
                    createAt: newdoc.createAt,
                }
            }
        }
 

    } catch (e) {
        console.log('Error: ' + e.message)
        console.log(e.message)
        return {
            status: 400,
            body: { error: 'Server Error' }
        }
    }

   
    /*
        async function deleteFile() {
            await bucket.file('req.file.originalname').delete();
          
            console.log(`req.file.originalname deleted`);
          }
          
        //deleteFile().catch(console.error);
        */

}




const delschema = Joi.object({
    id: Joi.string()
})
export async function del(request) {
    console.log('DELETE /sell ')
    try {
        const cookies = cookie.parse(request.headers.cookie || '')
        if (!cookies.id) throw new Error(`Invalid`)
        const body = await delschema.validateAsync(JSON.parse(request.body))
        console.log(body)
        const ref = db.collection('orders');
        const queryRef = ref.where('seller', '==', cookies.id)
            .where('id', '==', body.id)
            .where('status', '==', 0);

        const docs = await queryRef.get();
        if (docs.size == 0) throw new Error(`Không tìm thấy dữ liệu`)

        let idForDel = ''
        docs.forEach(async (doc) => {
            idForDel = doc.id
        })
        await db.collection('orders').doc(idForDel).update({ status: -1 })
        return {
            status: 200,
            body: { data: { id: idForDel } }
        }
    } catch (e) {
        console.log('Error: ' + e.message)
        console.log(e.message)
        return {
            status: 400
        }
    }
}


async function extract(img, img1) {
    const file = Buffer.from(img, 'base64');
    const [result] = await client.textDetection(file);
    const detections = result.textAnnotations
    const text = detections[0].description;

	let reId = /(?<=#.+)\d+/g
	let reRarity = /Common|Uncommon|Rare|Epic|Legendary/g
	let reType = /Walker|Jogger|Runner|Trainer/g
	let reLevel = /(?<=Level.+)\d+/g
	let reMint = /(?<=Mint.+)\d(?=\/\d)/g

    if(!text.match(reRarity) || !text.match(reType)) return null

	let [attr, base] = getAttribute(text)
    if(img1){
        const file1 = Buffer.from(img1, 'base64');
        const [result1] = await client.textDetection(file1);
        const detections1 = result1.textAnnotations
        const text1 = detections1[0].description;
        let [attr1, base1] = getAttribute(text1)
        let at, bs
        if(base == base1){
            return {
                idshoe: text.match(reId)[0],
                rarity: text.match(reRarity)[0],
                type: text.match(reType)[0],
                level: text.match(reLevel)[0],
                mint: text.match(reMint)[0],
                attributes: attr,
                base: base
            }
        }
        if (base < base1) {
            at = attr,
            bs = base
        }else {
            at = attr1,
            bs = base1
        }
        return {
            idshoe: text.match(reId)[0],
            rarity: text.match(reRarity)[0],
            type: text.match(reType)[0],
            level: text.match(reLevel)[0],
            mint: text.match(reMint)[0],
            attributes: at,
            base: bs
        }
    }else{
        return {
            idshoe: text.match(reId)[0],
            rarity: text.match(reRarity)[0],
            type: text.match(reType)[0],
            level: text.match(reLevel)[0],
            mint: text.match(reMint)[0],
            attributes: attr,
            base: base
        }
    }
	

}

function getAttribute(text) {
	let reAttr = /(\d+\.\d)(?!(.+SOL|\/\d+))/g
	let attr = text.match(reAttr)
	let attrLenght = attr.length
	attr = [attr[attrLenght - 4], attr[attrLenght - 3],attr[attrLenght - 2], attr[attrLenght - 1]]
	let base = Number(attr[0]) + parseInt(attr[3])
	
	return [attr, base]

}