import cookie from 'cookie';
import { db } from '../../gc'
import { FieldValue } from 'firebase-admin/firestore'
import { pnetwallet } from '../connection'
import {
    Keypair, LAMPORTS_PER_SOL, SystemProgram,
    Transaction,
    PublicKey,
    sendAndConfirmTransaction,
    Connection,
    clusterApiUrl
} from "@solana/web3.js";
import base58 from 'bs58'
import Joi from 'joi'

const connection = new Connection(clusterApiUrl(import.meta.env.VITE_SOLANA_NETWORK), 'confirmed');
export async function patch(request) {
    console.log('get /order ')
    try {
        const cookies = cookie.parse(request.headers.cookie || '')
        if (!cookies.id) throw new Error(`Invalid`)

        const ref = db.collection('orders');
        const queryRef = ref.where('seller', '==', cookies.id).where('status', '>', 0);
        const queryRef1 = ref.where('buyer', '==', cookies.id).where('status', '>', 0);

        const docs = await queryRef.get();
        const docs1 = await queryRef1.get();
        let arrayData = []
        docs.forEach(doc => {
            let data = doc.data()
            arrayData.push({
                own: true,
                _updateTime: doc._updateTime._nanoseconds,
                createAt: data.createAt,

                level: data.level,
                rarity: data.rarity,
                type: data.type,
                base: data.base,

                id: data.id,
                imgs: data.imgs,
                price: data.price,
                status: data.status,
            });
        });
        docs1.forEach(doc => {
            let data = doc.data()
            arrayData.push({
                own: true,
                _updateTime: doc._updateTime._nanoseconds,
                createAt: data.createAt,

                level: data.level,
                rarity: data.rarity,
                type: data.type,
                base: data.base,

                id: data.id,
                imgs: data.imgs,
                price: data.price,
                status: data.status,
            });
        });
        return { body: { data: arrayData } }

    } catch (e) {
        console.log('Error: ' + e.message)
        console.log(e.message)
        return {
            status: 400,
            body: { error: 'Server Error' }
        }
    }
}


const schemaPut = Joi.object({
    status: Joi.number(),
    id: Joi.string(),
    pub: Joi.string().empty(''),
})
export async function put(request) {
    console.log('post /order')
    try {
        const cookies = cookie.parse(request.headers.cookie || '')
        if (!cookies.id) throw new Error(`Invalid`)
        const body = await schemaPut.validateAsync(request.body)
        console.log(body)
        switch (body.status) {
            case 1: {
                const data = await getInfo(body.id)
                if (data.status != 0) throw new Error('Không tìm thấy dữ liệu')


                const userRef = db.collection('users').doc(cookies.id);
                const orderRef = db.collection('orders').doc(body.id);
                const messageRef = db.collection('messages').doc(body.id);

                const userDoc = await userRef.get();
                const userData = userDoc.data();
                const userPub = new PublicKey(base58.decode(userData.pub));

                const balance = (await connection.getBalance(userPub)) / LAMPORTS_PER_SOL
                console.log(balance)
                if (balance < (userData.lock + data.price)) throw new Error('Số dư không đủ')

                const batch = db.batch();
                batch.update(userRef, { lock: round5(userData.lock + data.price) });
                batch.update(orderRef, { buyer: cookies.id, buyeraddress: body.pub, status: 1, });
                batch.update(messageRef,
                    { status: 1, msg: FieldValue.arrayUnion({ msg: "NGƯỜI MUA đã thanh toán. NGƯỜI BÁN hãy chuyển giày theo địa chỉ ví người mua cũng cấp.", time: Date.now(), type: 'bot' }) }
                )
                await batch.commit();
            }; break;
            case 2: {
                const data = await getInfo(body.id)
                if (data.status != 1 || data.seller != cookies.id) throw new Error('Không tìm thấy dữ liệu')


                const orderRef = db.collection('orders').doc(body.id);
                const messageRef = db.collection('messages').doc(body.id);
                const batch = db.batch();
                batch.update(orderRef, { status: 2 });
                batch.update(messageRef, { status: 2, msg: FieldValue.arrayUnion({ msg: "Người bán đã thông báo chuyển giày, NGƯỜI MUA hãy kiểm tra giày trong ví của bạn.", time: Date.now(), type: 'bot' }) })

                await batch.commit();
            }; break;
            case 3: {
                const data = await getInfo(body.id)
                //if (data.status != 2 || data.buyer != cookies.id) throw new Error('Không tìm thấy dữ liệu')

                const userRef = db.collection('users').doc(cookies.id);
                const orderRef = db.collection('orders').doc(body.id);
                const messageRef = db.collection('messages').doc(body.id);

                const userDoc = await userRef.get();
                const userData = userDoc.data();

                const userKeyPair = Keypair.fromSecretKey(base58.decode(userDoc.data().key))

                const txToPnet = await sendTransaction(userKeyPair, pnetwallet.publicKey, data.price)
                console.log(txToPnet)
                const userSellerDoc = await db.collection('users').doc(data.seller).get();
                console.log(userSellerDoc.data())
                const payForSeller = round5(data.price * 99 / 100)
                
                const sellerPub = new PublicKey(base58.decode(userSellerDoc.data().pub))
                console.log('sellerPub.toBase58()')
                const txSeller = await sendTransaction(pnetwallet, sellerPub, payForSeller)
                console.log('txSeller')
                console.log(txSeller)


                if (userSellerDoc.data().ref) {
                    const payForSellerReference = round5(data.price * 0.2 / 100)
                    const sellerRefPub = new PublicKey(base58.decode(userSellerDoc.data().ref))
                    const txToSellerReference = await sendTransaction(pnetwallet, sellerRefPub, payForSellerReference)
                    console.log('txToSellerReference')
                    console.log(txToSellerReference)
                }

                const batch = db.batch();

                batch.update(userRef, { lock: round5(userData.lock + data.price)})
                batch.update(orderRef, { status: 3 });
                batch.update(messageRef, { status: 3, msg: FieldValue.arrayUnion({ msg: "NGƯỜI BÁN đã nhận được giày. PNET đã mở khoá số lượng SOL tương ứng cho người mua.", time: Date.now(), type: 'bot' }) })

                await batch.commit();
            }; break;
            case 4: {
                const data = await getInfo(body.id)
                if (data.status < 1 || data.status ==3) throw new Error('Lỗi')
                if (data.seller != cookies.id && data.buyer != cookies.id) throw new Error('Không tìm thấy dữ liệu')

                let msg
                if (data.seller == cookies.id) {
                    msg = FieldValue.arrayUnion({ msg: "Đang khiếu nại, NGƯỜI BÁN hãy trình bày lý do mà bạn khiếu nại.", time: Date.now(), type: 'bot' })
                }
                if (data.buyer == cookies.id) {
                    msg = FieldValue.arrayUnion({ msg: "Đang khiếu nại, NGƯỜI MUA hãy trình bày lý do mà bạn khiếu nại.", time: Date.now(), type: 'bot' })
                }
                const orderRef = db.collection('orders').doc(body.id);
                const messageRef = db.collection('messages').doc(body.id);

                const batch = db.batch();
                batch.update(orderRef, { status: 4 });
                batch.update(messageRef, { status: 4, msg: msg })

                await batch.commit();
            }; break;
        }

        return {
            status: 200,
            body: {
                data: body
            }
        }

    } catch (e) {
        console.log('Error: ' + e.message)
        return {
            status: 400,
            body: { error: 'Server Error' }
        }
    }

}

const getInfo = async (id) => {
    const doc = await db.collection('orders').doc(id).get()
    if (doc.empty) throw new Error(`Không tìm thấy dữ liệu`)
    const data = doc.data()
    return data
}

function round5(num) {
    return Math.round(num * 100000) / 100000;
}

async function sendTransaction(from, topublicKey, amount) {
    const transferTransaction = new Transaction().add(
        SystemProgram.transfer({
            fromPubkey: from.publicKey,
            toPubkey: topublicKey,
            lamports: (amount - 0.000005) * LAMPORTS_PER_SOL
        })
    );

    const result = await sendAndConfirmTransaction(connection, transferTransaction, [
        from,
    ]);
    return result;
}