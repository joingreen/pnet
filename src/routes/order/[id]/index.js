import cookie from 'cookie';
import { db } from '../../../gc'

export async function patch(request) { 
    console.log('GET /order/[id]')
    console.log(request.params)
   
    try {
        const cookies = cookie.parse(request.headers.cookie)
        if (!cookies.id) throw new Error(`Invalid`)
        const ref = db.collection('orders');

        const doc = await ref.doc(request.params.id).get()
        if (doc.empty) throw new Error(`No Content`)

        const data = doc.data()

        if (data.status>0 && data.buyer != cookies.id && data.seller != cookies.id){
             throw new Error("No content")
        }
       //console.log(doc)
   
        return {
            status: 200,
            body: {
                data: {
                    own: data.seller == cookies.id ? true : false,
                    _updateTime: doc._updateTime._seconds,
                    createAt: data.createAt,

                    level: data.level,
                    rarity: data.rarity,
                    type: data.type,
                    base:data.base,

                    id:data.id,
                    imgs: data.imgs,
                    price: data.price,
                    status: data.status,
                    buyeraddress:data.buyeraddress
                }
            }
        }
    } catch (e) {
        console.log('Error: ' + e.message)
        console.log(e)
        return {
            status: 400,
            body: { error: 'Error' }
        }
    }
}
