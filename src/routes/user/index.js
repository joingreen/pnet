import { db } from "../../gc"
import cookie from 'cookie';
import { Keypair, LAMPORTS_PER_SOL } from "@solana/web3.js";
import connection from '../connection'
import base58 from 'bs58'
import { sha256 } from 'js-sha256';
import Joi from 'joi'


export const patch = async (request) => {
    console.log('GET /user ')
    const body = JSON.parse(request.body)
    let id =  ''
    if(body) {
        id = body.id
    }
    try {
        const cookies = cookie.parse(request.headers.cookie || '')
        if (!id) {id = cookies.id}
        if (!id) return {status:200, body: {error: 'Cookie not found'}}

        const doc = await db.collection('users').doc(id).get()
        if (doc.empty) return {status:200, body: {error: 'Token not found'}}
        const user = doc.data()
        return {
            status: 200,
            headers: {
                'Set-Cookie': cookie.serialize('id', doc.id, {
                    path: '/',
                    httpOnly: true,
                    expires: new Date(Date.now() + 1000 * 60 * 60 * 24 * 365)
                })
            },
            body: { pub: user.pub, lock: user.lock, history: user.history }
        }

    } catch (e) {
        console.log(e.message)
        return {
            status: 400,
            messageText: e.message
        }
    }
}


const schema = Joi.object({
    ref: Joi.string().empty('')
})

export const post = async (request) => {
    console.log('post user')
    try {
        const body = await schema.validateAsync(request.body)
        const keypair = Keypair.generate();
        //requestAirdrop  START <
        const airdropSignature = await connection.requestAirdrop(
            keypair.publicKey,
            LAMPORTS_PER_SOL,
        );

        await connection.confirmTransaction(airdropSignature);
        //requestAirdrop   END>
        const hash = sha256(`${keypair.publicKey.toBase58()}lMmkll!1${base58.encode(keypair.secretKey)}`)
        await db.collection('users').doc(hash).set({ ref: body.ref || '', key: base58.encode(keypair.secretKey), lock: 0, pub: keypair.publicKey.toBase58(), history: [] })

        return {
            status: 200,
            headers: {
                'Set-Cookie': cookie.serialize('id', hash, {
                    path: '/',
                    httpOnly: true,
                    expires: new Date(Date.now() + 1000 * 60 * 60 * 24 * 365)
                })
            },
            body: {
                 key: hash, lock: 0, pub: keypair.publicKey.toBase58(), history: [] 
            }
        }


    } catch (e) {
        console.log(e.message)
        return { status: 400 }
    }
}