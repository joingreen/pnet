import cookie from 'cookie';

export const patch = async (request) => {
    console.log('GET /auth ')
    try {
        const cookies = cookie.parse(request.headers.cookie || '')
        if (!cookies.id) {
            console.log('cookie id null')
            return {status: 200}
        }
        return {
            status: 200,
            headers: {
                'Set-Cookie': cookie.serialize('id', '', {
                    path: '/',
                    httpOnly: true,
                    expires: new Date(Date.now() + 1000)
                })
            }
            
        }

    } catch (e) {
        console.log(e.message)
        return {
            status:400
        }
    }
}