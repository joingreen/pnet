import { db } from "../../gc"
import cookie from 'cookie';
import {
    Keypair, LAMPORTS_PER_SOL, SystemProgram,
    Transaction,
    PublicKey,
    sendAndConfirmTransaction
} from "@solana/web3.js";
import base58 from 'bs58'
import connection from '../connection'
import Joi from 'joi'

const schema = Joi.object({
    address: Joi.string(),
    amount: Joi.number()
})

export const post = async (request) => {
    console.log('POST /user/withdraw ')
    try {
        const body = await schema.validateAsync(request.body)
        const cookies = cookie.parse(request.headers.cookie || '')
        if (!cookies.id) throw new Error(`Invalid`)

        const doc = await db.collection('users').doc(cookies.id).get()
        if (doc.empty) throw new Error(`Invalid`)
        
        const data = doc.data()
        let wallet = Keypair.fromSecretKey(base58.decode(data.key))

        let balance = (await connection.getBalance(wallet.publicKey)) / LAMPORTS_PER_SOL
        console.log(balance)

        let recieveWallet = new PublicKey(base58.decode(body.address))


        const transferTransaction = new Transaction()
            .add(SystemProgram.transfer({
                fromPubkey: wallet.publicKey,
                toPubkey: recieveWallet,
                lamports: body.amount * LAMPORTS_PER_SOL,
            }))
        let resultTransaction = await sendAndConfirmTransaction(connection, transferTransaction, [wallet]);

        let newTx = { txid: resultTransaction, createAt: Date.now() }
        let newhistory = data.history
        if (data.history.length > 10) {
            newhistory.shift()
        }
        newhistory.push(newTx)
        await db.collection('users').doc(cookies.id).update({ history: newhistory })
        return {
            status: 200,
            body: { data: { history: newhistory } }
        }
    } catch (e) {
        console.log('Error: ' + e.message)
        console.log(e.message)
        return {
            status: 400,
            body: { error: 'Server Error' }
        }
    }
}