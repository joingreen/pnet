import cookie from 'cookie';
import {db} from '../gc'



export async function patch(request) {
    try {
        console.log('GET / ')
        const cookies = cookie.parse(request.headers.cookie || '')
        if (!cookies.id) throw new Error(`Invalid`)

        const ref = db.collection('orders');
        const queryRef = ref.where('seller', '!=', cookies.id).where('status', '==', 0);

        const docs = await queryRef.get()
        if (docs.empty) return { body: { data: [] } }
        let arrayData = []
        docs.forEach(doc => {
            // console.log(doc._updateTime._seconds)
            let data = doc.data()
            arrayData.push({
                attr: data.attr,
                idshoe: data.idshoe,
                level: data.level,
                mint: data.mint,
                rarity: data.rarity,
                type: data.type,

                base: data.base,
                id: data.id,
                imgs: data.imgs,
                price: data.price,
                status: data.status,
                createAt: data.createAt
            });
        });
        return { body: { data: arrayData } }

    } catch (e) {
        console.log('Error: ' + e.message)
        console.log(e.message)
        return {
            status: 400
        }
    }
}
