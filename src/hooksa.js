import cookie from 'cookie';


export const handle = async ({ request, resolve }) => {
	
    const cookies = cookie.parse(request.headers.cookie || '')
	if(!cookies.userid) return {
        headers: { Location: '/login' },
        status: 302
    }
	if (request.query.has('_method')) {
		request.method = request.query.get('_method').toUpperCase();
	}
	request.locals.userid = cookies.userid
	const response = await resolve(request);
	return response;
};
