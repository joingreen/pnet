import pkg from 'winston';
const { transports: _transports, format: _format, createLogger } = pkg;

const logConfiguration = {
  transports: [
      new _transports.Console(),
      new _transports.File({ filename: 'error.log', level: 'error' }),
      new _transports.File({ filename: 'info.log', level: 'info' }),
      
  ],
  format: _format.combine(
      _format.label({
          label: `Label🏷️`
      }),
      _format.timestamp({
         format: 'MMM-DD-YYYY HH:mm:ss'
     }),
      _format.printf(info => `${info.level}: ${info.label}: ${[info.timestamp]}: ${info.message}`),
  )
};

export default createLogger(logConfiguration);