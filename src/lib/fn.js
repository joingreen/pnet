export const env = {
  SOLANA_ClUSTER:'devnet'
}

export const formatTime = (time) => {
  let d = new Date(time)
  let hArr = d.toLocaleString('vi-VN').split(':')
  let dayArr = hArr[2].split(' ')[1].split('/')
  return `${hArr[0]}:${hArr[1]} ${dayArr[0]}/${dayArr[1]}`
}

export const countDown = (time, cb) => {
  let updateT = time

  const countdown = (updateTime) => {
    const timeleft = (new Date(updateTime).getTime() + 20 * 60 * 1000) - Date.now()

    if (timeleft > 0) {
      let d = new Date(timeleft)
      let r = `${d.getMinutes() < 10 ? '0' + d.getMinutes() : d.getMinutes()}:${d.getSeconds() < 10 ? '0' + d.getSeconds() : d.getSeconds()}`
      return cb(r)
    } else {
      clearInterval(myfunc)
    }
  }
  const myfunc = setInterval(() => {
    countdown(updateT)
  }, 1000)
  countdown(updateT)
}