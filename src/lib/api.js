const base = 'http://192.168.1.111:3000';

import { goto } from '$app/navigation';
/**
 * @param {string} method
 * @param {string} resource
 * @param {Record<string, unknown>} [data]
 */
export async function api(method, resource, data, cb ) {
    //setisLoading(true)
    console.log(`${base}/${resource}` + method)
    const resp = await fetch(`${base}/${resource}`, {
		method,
		headers: {
			'content-type': 'application/json'
		},
		body: JSON.stringify(data || {})
	})

    console.log(resp)
    
   // setisLoading(false)
    if(resp.status == 200){
        const res= await resp.json()
        return cb(res)
       
        
    }
    if(resp.status == 302){
        goto(resp.url)
    }
    let message = ''
    if(resp.status == 400){
        const res= await resp.json()
        message = res.message || 'Không thể thực hiện'
    }

    if(resp.status == 500){
        message = 'Server bận, thử lại sau' 
    }
    if(resp.status == 403){
        message =  'Không tìm thấy dữ liệu'
    }

    cb(null, {code: resp.status, message: message || resp.statusText})
}