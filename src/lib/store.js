import { writable } from 'svelte/store'

const shopStoreS = () => {
	const { subscribe, set, update } = writable([])
	return {
		subscribe,
		set:(data) => set(data),
		update:(data) => update(current =>{
			return {...current, ...data}
		})
	}
}
export const shopStore = shopStoreS()

const filterStoreS = () => {
	const { subscribe, set, update } = writable([])
	return {
		subscribe,
		set:(data) => set(data),
		update:(data) => update(current =>{
			return {...current, ...data}
		})
	}
}
export const filterStore = filterStoreS()

const sellStoreS = () => {
	const { subscribe, set, update } = writable([])
	return {
		subscribe,
		set:(data) => set(data),
		update:(newdoc) => update(current=>{
			return [...current, ...newdoc ] 
		}),
	}
}
export const sellStore = sellStoreS()

const userStoreS = () => {
	const { subscribe, set, update } = writable({pub:'', sol:'',lock:'', history:[]})
	return {
		subscribe,
		set:(data) => set(data),
		update:(newdoc) => update(current=>{
			return { ...current, ...newdoc }
		}),
	}
}
export const userStore = userStoreS()

export const orderStoreS = () =>{
	const { subscribe, set, update } = writable([])
	return {
		subscribe,
		set:(data) => set(data),
		update:(newdata) => update(old=>[...old,newdata]),
		updateById:(doc) => update(current=>current.map(current=>{
			if(current.id === doc.id) {
				return doc
			}
			return current
		})),
		
	}
}
export const orderStore = orderStoreS()

const codeAlert = writable(0)
const messageAlert = writable('')

function setAlert(code, message) {
	messageAlert.set(message)
	codeAlert.set(code)
	setTimeout(()=>{
		codeAlert.set(0)
	},5000)
}
export {codeAlert, setAlert, messageAlert}

const isLoading = writable(false)

function setisLoading(value) {
	isLoading.set(value)
}
export {isLoading, setisLoading}