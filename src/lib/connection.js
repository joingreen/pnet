import {env} from '$lib/fn'
import {Connection, clusterApiUrl} from '@solana/web3.js'

export const connection = new Connection(clusterApiUrl(env.SOLANA_ClUSTER), 'confirmed');