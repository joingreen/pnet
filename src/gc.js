import serviceAccount from './sola.json'

import { initializeApp,  cert} from 'firebase-admin/app';
import { getFirestore} from 'firebase-admin/firestore';
import  {getStorage} from 'firebase-admin/storage';

initializeApp({
  credential: cert(serviceAccount),
  storageBucket: 'sola-e9c81.appspot.com'
});

const db = getFirestore()

const storage = getStorage();
const bucket = storage.bucket();

export {db, bucket}