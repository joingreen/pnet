FROM node:17-alpine3.12
EXPOSE 3000
COPY . /app
WORKDIR /app
RUN npm init -y
RUN npm install
RUN npm run build

CMD node build/index.js

