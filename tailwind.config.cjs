//const colors = require('tailwindcss/colors')

module.exports = {
	content: ['./src/**/*.svelte', './src/**/*.css'],
	darkMode: `media`,
	theme: {
        extend: {
            colors: {
                buy: '#4eb893',
				sell: '#ff9200',
				success:'#d1e7dd',
				warning:'#fff3cd',
				danger:'#f8d7da',
				a:'#8C3590',
				b:'#DE640B',
				c:'#F5F100',
				d:'#0BDE85',
				e:'#2C3182',
				o:'#F9AC19',
				link:'#1072eb',
				menu:'#9CF8B5',
				stepnbuy:'#6EFCCB',
				stepntext:'#353E3B'

            },
			boxShadow: {
				n:'6px 6px 12px #b8b9be,-6px -6px 12px #fff!important',
				i:'inset 2px 2px 5px #b8b9be, inset -3px -3px 7px #fff!important'
			}
        },
	},
	variants: {
		extend: {inset: ['active']},
	},
	plugins: [],
}